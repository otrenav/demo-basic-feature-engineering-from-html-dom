
# Login Page Detection

## Goal

The goal is to show how simple features can be extracted from DOM and saved into
CSVs to later be used in ML models. The design, code, and documentation are very
rough, and are meant just for conceptual purposes, not real systems.

Send questions to Omar (omar.trejo@gigsternetwork.com).

## Process

1. *Specify pages to be processed*
    - For this demo, piicked some examples from
        - https://www.prismmoney.com/directory
2. *Specify feature combinations*
    - For this demo, tag: names, attributes, values
3. *Generate feature specifications*
    - For this demo, use simple combinations
4. *Extract features from pages*
    - CSV inputs and outputs

- Tests are included for the feature extraction

## Some cases

- Static pages with shown login forms
- Static pages with hidden login forms
- Sensitivity to query parameters
- Dynamic login forms
- Async login forms
- Timeouts

## Feature Engineering

### Specific and Generic Features

Some features may be specific, and if these are present, it is hihgly likely
that the page will contain a login form, which is assumed to be a login page.
For example, a button with the text "Log in" inside would be a specific feature.

Some features may be generic, meaning that if they are present there may be an
indication of a login form somewhere in the page but the more precise features
were not detected. This follows the idea that "some information" is better than
"no information" to train the ML models to detect login pages. On the most
generic extreme, strings will be matched anywhere they appear on an HTML
document (e.g. tag names, tag texts, IDs, classes, etc).

### Examples

- See the demo code for many more examples

- Presence of strings anywhere (e.g. sign up, forgot password, ...)
- Input tag with text in ID (e.g. user, password, account, email, login, ...)
- Input tags with text in value (e.g. Log in, Sign in, ...)
- Button tags with text (e.g. Log in, Sign in, ...)
- Labels tags with text (e.g. user, account, ...)

- There are many tag attributes to explore (e.g. value, for, name, title, ...)
- Note also that AND/OR mixed conditionals are possible (can use functions)

## Tests

- Total sites: 13 (home pages)
- Total pages: 23 (includes navigation to login pages)

## Expected successes

- [x] advanceddisposal.com
    - ^ Blocking pop-up after "PAY MY BILL" is clicked
    - [x] secure8.i-doxs.net/AdvancedDisposal/SignIn.aspx
        - ^ Non-HTTPS requests hang with timeouts
- [x] aum-inc.com
    - ^ Hidden form
- [x] cashnetusa.com
- [x] hrsd.com
    - ^ Seems to appear an disappear (DNS can't reach it), includes below
    - [x] invoicecloud.com/portal/(S(1c0ft3i0q0iwrdlwvariy2jd))/2/Site2.aspx
    - [x] invoicecloud.com/portal/(S(3h0w42uuvdudrt5031monfeq))/2/customerlogin.aspx
    - [x] invoicecloud.com/portal/(S(3h0w42uuvdudrt5031monfeq))/2/customerlogin.aspx?billerguid=52785305-4c7c-4bbe-9b3b-3d5f2fc7aeb9
        - ^ Sensitive to query parameters
        - ^ Sensitive to HTTPS
- [x] ouc.com
- [x] pascocountyfl.net
    - [x] pascocountyfl.net/MyAccount/
    - [x] account.civicplus.com/identity/CivicPlusLogin
- [x] samsclub.com
    - [x] samsclub.com/sams/account/signin/login.jsp
- [x] srpnet.com
- [x] trashbilling.com
- [x] wasteindustries.com
    - [x] wasteindustries.com/myaccount/login

## Expected failures

- Picked some examples from: https://www.prismmoney.com/directory

- [x] banfield.com: injects form after click (built with Angular)
- [x] rentacenter.com: async fetching of login form
- [x] mlgw.com
    - [x] mlgw.com/MyMLGWaccount
    - [x] mymlgw.mlgw.org
        - ^ Timeout

## Notes

- Some current features are logical impossibilities (e.g. button/password)
    - Feature sparsity can be avoided with smarter generation design
- We can add counts to features to improve false positive detection
- Smarter keyword combination mechanisms can be used later

## Usage

### Feature Generation

```
$ python login_page/generate_features.py
```

### Feature Extraction

```
$ python login_page/extract_features.py \
    --inputs inputs/pages.csv \
    --outputs outputs/pages.csv
```

### Testing

```
$ pytest -vv
```
