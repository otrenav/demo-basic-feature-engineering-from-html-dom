import json
import itertools
import constants as c


def generate_features():
    features = []
    for combo in itertools.product(c.TAG_NAMES, c.TAG_ATTRS, c.DOM_KEYWORDS):
        features.append(feature(combo[0], combo[1], combo[2]))
    return features


def feature(name, attr, keyword):
    spec = {"name": name}
    if attr == "string":
        spec["string"] = keyword
    else:
        spec["attrs"] = {attr: keyword}
    return {"id": f"{name}_{attr}_{keyword}", "spec": spec}


def save_to_outputs(features, fname):
    with open(f"./outputs/{fname}", "w") as file_:
        json.dump(features, file_)


if __name__ == "__main__":
    features = generate_features()
    save_to_outputs(features, "features.json")
    print("Generating features... DONE")
