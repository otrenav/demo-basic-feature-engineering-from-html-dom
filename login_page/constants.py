import json

TAG_NAMES = ["a", "input", "label", "button"]

TAG_ATTRS = ["id", "value", "name", "title", "string"]

DOM_KEYWORDS = [
    "account",
    "customer",
    "email",
    "login",
    "password",
    "signin",
    "uid",
    "user",
    "userid",
    "username",
]

TEXT_KEYWORDS = DOM_KEYWORDS + [
    "forgot email",
    "forgot password",
    "forgot user",
    "forgot username",
    "forgot your email",
    "remember me",
    "remember my email",
]


with open("outputs/features.json", "r") as file_:
    AVAILABLE_FEATURES = json.load(file_)
