import pytest

from . import constants_tests as t
from . import constants as c
from .page import Page


@pytest.mark.parametrize("url,expected_features", t.TEST_SIMPLE_FEATURES_INPUTS)
def test_simple_features(url, expected_features):
    detected_features = []
    page = Page(url)
    for feature in c.AVAILABLE_FEATURES:
        if page.feature_exists(feature["spec"]):
            detected_features.append(feature["id"])
    assert expected_features == detected_features
