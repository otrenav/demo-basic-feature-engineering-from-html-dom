import re
import copy
import requests

from bs4 import BeautifulSoup


class Page:
    def __init__(self, url):
        self.spec = None
        self.page = requests.get(url)
        self.dom = BeautifulSoup(self.page.content, "html.parser")

    @property
    def txt(self):
        return self.page.text

    def text_contains(self, string):
        return self.txt.lower().find(string) != -1

    def feature_exists(self, spec):
        self.spec = copy.deepcopy(spec)
        self._limit_results_to_increase_speed()
        self._subsitute_keywords_with_broad_regex()
        return self._at_least_one_result(self.dom.find_all(**self.spec))

    def _limit_results_to_increase_speed(self, n=1):
        self.spec["limit"] = n

    def _subsitute_keywords_with_broad_regex(self):
        if "string" in self.spec:
            pattern = f".*{self.spec['string']}.*"
            self.spec["string"] = re.compile(pattern, flags=re.IGNORECASE)
        else:
            # NOTE: assumption: single key in attrs
            key = list(self.spec["attrs"].keys())[0]
            pattern = f".*{self.spec['attrs'][key]}.*"
            self.spec["attrs"][key] = re.compile(pattern, flags=re.IGNORECASE)

    def _at_least_one_result(self, results):
        return len(results) >= 1

    def _checkbox_followed_by_persistence(self):
        # TODO: Check sibling after all checkboxes
        # TODO: Introduce other persistence strings
        res = self.dom.find_all(name="input", attrs={"type": "checkbox"})
        res = res[0].next_sibling.find_all(
            string=re.compile("stay signed in", flags=re.IGNORECASE)
        )
