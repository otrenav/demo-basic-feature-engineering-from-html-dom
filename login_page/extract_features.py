import pandas
import argparse

import constants as c
from page import Page


def parse_args():
    parser = argparse.ArgumentParser(
        description="Extract features from pages CSV"
    )
    parser.add_argument(
        "--inputs",
        type=str,
        help="Input CSV with `url` column",
        default="inputs/pages.csv",
    )

    parser.add_argument(
        "--outputs",
        type=str,
        help="Output CSV with page features",
        default="outputs/pages.csv",
    )
    return parser.parse_args()


def read_data(inputs):
    print(data)
    return data


def add_features(data):
    urls = list(data.url)
    n = len(urls)
    for i, url in enumerate(urls):
        print(f"- [{i}/{n}] {url}")
        try:
            page = Page(url)
        except Exception as e:
            print(e)
        else:
            for feature in c.AVAILABLE_FEATURES:
                exists = page.feature_exists(feature["spec"])
                data.loc[i, feature["id"]] = exists
                if exists:
                    print(f"    - {feature['id']}")
    return data


if __name__ == "__main__":
    print("")
    args = parse_args()
    data = pandas.read_csv(args.inputs)
    data = add_features(data)
    data.to_csv(args.outputs, index=False)
    print("\nExtracing features... DONE\n")
